package model

type Plurality int

const (
	Optional Plurality = iota
	Single
	Many
)

func (f *Field) Plurality() Plurality {
	switch {
	case f.MinCount == 0 && f.MaxCount == 1:
		return Optional
	case f.MinCount == 1 && f.MaxCount == 1:
		return Single
	default:
		return Many
	}
}
