package model

type (
	Model struct {
		XMLName  struct{} `xml:"model"`
		Entities []Entity
	}

	Entity struct {
		XMLName struct{} `xml:"entity"`
		Name    string   `xml:"name,attr"`
		Fields  []Field
	}

	Field struct {
		XMLName  struct{} `xml:"field"`
		Name     string   `xml:"name,attr"`
		Type     string   `xml:"type,attr"`
		MinCount int      `xml:"min-count,attr"`
		MaxCount int      `xml:"max-count,attr"`
	}
)
