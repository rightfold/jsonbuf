package main

import (
	"fmt"

	"bitbucket.org/rightfold/jsonbuf/ecmascript"
	"bitbucket.org/rightfold/jsonbuf/model"
)

func main() {
	model := model.Model{
		Entities: []model.Entity{
			{
				Name: "User",
				Fields: []model.Field{
					{
						Name:     "id",
						Type:     "UUID",
						MinCount: 1,
						MaxCount: 1,
					},
					{
						Name:     "name",
						Type:     "String",
						MinCount: 0,
						MaxCount: 1,
					},
					{
						Name:     "age",
						Type:     "Integer",
						MinCount: 1,
						MaxCount: 1,
					},
					{
						Name:     "pets",
						Type:     "String",
						MinCount: 0,
						MaxCount: -1,
					},
					{
						Name:     "tags",
						Type:     "String",
						MinCount: 1,
						MaxCount: 5,
					},
				},
			},
		},
	}
	fmt.Println(ecmascript.Convert(&model))
}
