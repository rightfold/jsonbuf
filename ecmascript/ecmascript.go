package ecmascript

import (
	"bytes"
	"fmt"

	"bitbucket.org/rightfold/jsonbuf/model"
)

func Convert(model *model.Model) string {
	b := new(bytes.Buffer)
	fmt.Fprintf(b, "var $jsonbuf = require('jsonbuf');\n")
	for _, entity := range model.Entities {
		convertEntity(b, &entity)
	}
	return b.String()
}

func convertEntity(b *bytes.Buffer, entity *model.Entity) {
	fmt.Fprintf(b, "function ")
	fmt.Fprintf(b, entity.Name)
	fmt.Fprintf(b, "(")
	for i, field := range entity.Fields {
		if i != 0 {
			fmt.Fprintf(b, ", ")
		}
		fmt.Fprintf(b, field.Name)
	}
	fmt.Fprintf(b, ") {\n")
	for _, field := range entity.Fields {
		fmt.Fprintf(b, "    this.%s = %s;\n", field.Name, field.Name)
	}
	fmt.Fprintf(b, "    this._validate();\n")
	fmt.Fprintf(b, "}\n")

	fmt.Fprintf(b, "%s.prototype._validate = function() {\n", entity.Name)
	for _, field := range entity.Fields {
		if field.Plurality() == model.Single {
			fmt.Fprintf(b, "    if (this.%s === null) {\n", field.Name)
			fmt.Fprintf(b, "        throw new $jsonbuf.ValidationError('field %s must not be null');\n", field.Name)
			fmt.Fprintf(b, "    }\n")
		}

		if field.Plurality() == model.Many {
			fmt.Fprintf(b, "    if (!(this.%s instanceof $jsonbuf.Array)) {\n", field.Name)
			fmt.Fprintf(b, "        throw new $jsonbuf.ValidationError('field %s must be an array');\n", field.Name)
			fmt.Fprintf(b, "    }\n")

			if field.MinCount != 0 {
				fmt.Fprintf(b, "    if (this.%s.length < %d) {\n", field.Name, field.MinCount)
				fmt.Fprintf(b, "        throw new $jsonbuf.ValidationError('field %s must have at least %d elements');\n", field.Name, field.MinCount)
				fmt.Fprintf(b, "    }\n")
			}

			if field.MaxCount != -1 {
				fmt.Fprintf(b, "    if (this.%s.length > %d) {\n", field.Name, field.MaxCount)
				fmt.Fprintf(b, "        throw new $jsonbuf.ValidationError('field %s must have at most %d elements');\n", field.Name, field.MaxCount)
				fmt.Fprintf(b, "    }\n")
			}
		}

		switch field.Plurality() {
		case model.Optional:
			fmt.Fprintf(b, "    var values;\n")
			fmt.Fprintf(b, "    if (this.%s === null) {\n", field.Name)
			fmt.Fprintf(b, "        values = [];\n")
			fmt.Fprintf(b, "    } else {\n")
			fmt.Fprintf(b, "        values = [this.%s];\n", field.Name)
			fmt.Fprintf(b, "    }\n")
		case model.Single:
			fmt.Fprintf(b, "    var values = [this.%s];\n", field.Name)
		case model.Many:
			fmt.Fprintf(b, "    var values = this.%s;\n", field.Name)
		}
		fmt.Fprintf(b, "    for (var i = 0; i < values.length; ++i) {\n")
		var check string
		switch field.Type {
		case "Boolean":
			check = "typeof values[i] !== 'boolean'"
		case "Float":
			check = "typeof values[i] !== 'number'"
		case "Integer":
			check = "typeof values[i] !== 'number' || values[i] % 1 !== 0"
		case "String":
			check = "typeof values[i] !== 'string'"
		default:
			check = "!(values[i] instanceof " + field.Type + ")"
		}
		fmt.Fprintf(b, "        if (%s) {\n", check)
		fmt.Fprintf(b, "            throw new $jsonbuf.ValidationError('field %s must be a(n) %s');\n", field.Name, field.Type)
		fmt.Fprintf(b, "        }\n")
		fmt.Fprintf(b, "    }\n")
	}
	fmt.Fprintf(b, "};\n")

	fmt.Fprintf(b, "%s.toJSON = function(entity) {\n", entity.Name)
	fmt.Fprintf(b, "    %s.prototype._validate.call(entity);\n", entity.Name)
	fmt.Fprintf(b, "    var json = {};\n")
	for _, field := range entity.Fields {
		fmt.Fprintf(b, "    json.%s = $jsonbuf.toJSON(entity.%s);\n", field.Name, field.Name)
	}
	fmt.Fprintf(b, "    return json;\n")
	fmt.Fprintf(b, "};\n")
}
