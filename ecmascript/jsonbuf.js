module.exports.ValidationError = Error;

module.exports.toJSON = function(entity) {
    switch (typeof entity) {
    case 'boolean':
    case 'number':
    case 'string':
        return entity;
    case 'object':
        if (entity === null) {
            return null;
        } else if (entity instanceof Array) {
            return entity.map(module.exports.toJSON);
        } else if (typeof entity.constructor === 'function'
                    && typeof entity.constructor.toJSON === 'function') {
            return entity.constructor.toJSON(entity);
        }
        /* fallthrough */
    default:
        throw new ValidationError('cannot convert ' + typeof entity + ' to JSON')
    }
};
